package com.blackphoenixproductions.notificationservice.domain;

import com.blackphoenixproductions.notificationservice.domain.model.Post;

public interface INotificationService {
    void notifyTopicAuthor(Post post);
}
