package com.blackphoenixproductions.notificationservice.domain.factory;


import com.blackphoenixproductions.notificationservice.domain.model.Notification;
import com.blackphoenixproductions.notificationservice.domain.model.Post;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class NotificationSimpleFactory {

    private static final int MAX_NOTIFICATION_LENGTH = 20;


    public Notification createUserNotification(Post post){
        Notification notification = createNotification(post);
        notification.setUrl("/viewtopic?id=" + post.getTopic().getId() + "&page=" + post.getLastPageNumber());
        return notification;
    }

    private Notification createNotification(Post post){
        Notification notification = new Notification();
        notification.setFromUser(post.getUser());
        notification.setToUser(post.getTopic().getUser());
        notification.setFromUser(post.getUser());
        notification.setPost(post);
        notification.setCreateDate(LocalDateTime.now());
        notification.setMessage(setNotificationMessage(post.getMessage()));
        return notification;
    }

    private String setNotificationMessage(String message) {
        int messageSize = message.length();
        if (messageSize > MAX_NOTIFICATION_LENGTH) {
            message = message.substring(0, MAX_NOTIFICATION_LENGTH);
            message += " [...]";
        }
        return message;
    }
}
