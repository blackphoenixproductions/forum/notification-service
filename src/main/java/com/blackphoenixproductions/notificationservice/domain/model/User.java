package com.blackphoenixproductions.notificationservice.domain.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class User {
    @EqualsAndHashCode.Exclude
    private Long id;
    private String username;
    private String email;
    private String role;
}
