package com.blackphoenixproductions.notificationservice.domain.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Notification {
    private String id;
    private User fromUser;
    private User toUser;
    private Post post;
    private String message;
    private String url;
    private LocalDateTime createDate;
}
