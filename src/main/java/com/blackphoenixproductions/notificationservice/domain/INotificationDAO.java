package com.blackphoenixproductions.notificationservice.domain;

import com.blackphoenixproductions.notificationservice.domain.model.Notification;

import java.util.List;

public interface INotificationDAO {
    void saveNotification(Notification notification);
    List<Notification> getUserNotification(String username);
    Boolean getUserNotificationStatus(String username);
    void setNotificationStatus(String username, boolean showNotificationNotice);
}
