package com.blackphoenixproductions.notificationservice.domain.service;

import com.blackphoenixproductions.notificationservice.domain.INotificationDAO;
import com.blackphoenixproductions.notificationservice.domain.INotificationService;
import com.blackphoenixproductions.notificationservice.domain.ISSEPushNotificationService;
import com.blackphoenixproductions.notificationservice.domain.factory.NotificationSimpleFactory;
import com.blackphoenixproductions.notificationservice.domain.model.Notification;
import com.blackphoenixproductions.notificationservice.domain.model.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationService implements INotificationService {

    private final NotificationSimpleFactory notificationSimpleFactory;
    private final ISSEPushNotificationService pushNotificationService;
    private final INotificationDAO notificationDAO;

    @Autowired
    public NotificationService(NotificationSimpleFactory notificationSimpleFactory, ISSEPushNotificationService pushNotificationService, INotificationDAO notificationDAO) {
        this.notificationSimpleFactory = notificationSimpleFactory;
        this.pushNotificationService = pushNotificationService;
        this.notificationDAO = notificationDAO;
    }

    @Override
    public void notifyTopicAuthor(Post post) {
        Notification notification = notificationSimpleFactory.createUserNotification(post);
        notificationDAO.saveNotification(notification);
        pushNotificationService.sendNotification(notification.getToUser().getUsername());
    }

}
