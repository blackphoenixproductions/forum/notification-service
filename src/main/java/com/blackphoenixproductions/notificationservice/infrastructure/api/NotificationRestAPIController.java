package com.blackphoenixproductions.notificationservice.infrastructure.api;


import com.blackphoenixproductions.notificationservice.domain.INotificationDAO;
import com.blackphoenixproductions.notificationservice.domain.model.Notification;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/posts")
@Tag(name = "1. Notification", description = "endpoints riguardanti le notifiche.")
public class NotificationRestAPIController {

    private static final Logger logger = LoggerFactory.getLogger(NotificationRestAPIController.class);

    private final INotificationDAO notificationDAO;

    @Autowired
    public NotificationRestAPIController(INotificationDAO notificationDAO) {
        this.notificationDAO = notificationDAO;
    }

    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
    })
    @Operation(summary = "Restituisce tutte le notifiche di un utente.")
    @GetMapping(value = "getUserNotificationList")
    public ResponseEntity<List<Notification>> getUserNotificationList(@Parameter(description = "L'username dell'utente.") @RequestParam String username, HttpServletRequest req){
        logger.info("Start getUserNotificationList");
        List<Notification> notifications = notificationDAO.getUserNotification(username);
        logger.info("End getUserNotificationList");
        return new ResponseEntity<List<Notification>>(notifications, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
    })
    @Operation(summary = "Restituisce lo status delle notifiche di un utente. Permette di capire se un utente ha almeno una notifica da leggere.")
    @GetMapping(value = "getUserNotificationStatus")
    public ResponseEntity<Boolean> getUserNotificationStatus(@Parameter(description = "L'username dell'utente.") @RequestParam String username, HttpServletRequest req){
        logger.info("Start getUserNotificationStatus");
        Boolean notificationStatus = notificationDAO.getUserNotificationStatus(username);
        logger.info("End getUserNotificationStatus");
        return new ResponseEntity<Boolean>(notificationStatus, HttpStatus.OK);
    }

    @PreAuthorize("hasAnyAuthority('USER', 'STAFF', 'HELPDESK')")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "OK"),
    })
    @Operation(summary = "Imposta lo status delle notifiche di un utente.")
    @PostMapping(value = "setNotificationStatus")
    public ResponseEntity<String> setNotificationStatus(HttpServletRequest req,
                                                        @Parameter(description = "Determina se mostrare l'avviso di una nuova notifica da leggere.") @RequestParam boolean showNotificationNotice,
                                                        @Parameter(description = "L'username dell'utente.") @RequestParam String username){
        logger.info("Start setNotificationStatus");
        notificationDAO.setNotificationStatus(username, showNotificationNotice);
        logger.info("End setNotificationStatus");
        return new ResponseEntity<String>("Le notifiche sono state lette.", HttpStatus.OK);
    }

}
