package com.blackphoenixproductions.notificationservice.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@AllArgsConstructor
@Data
@RedisHash("NotificationStatusEntity")
public class NotificationStatusEntity {

    @Id
    private String username;
    private Boolean newNotification;
}
