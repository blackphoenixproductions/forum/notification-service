package com.blackphoenixproductions.notificationservice.infrastructure.entity;

import jakarta.persistence.GeneratedValue;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;
import org.springframework.data.redis.core.index.Indexed;

import java.time.LocalDateTime;

@Data
@RedisHash("NotificationEntity")
public class NotificationEntity {

    @Id
    @GeneratedValue
    private String id;
    @Indexed
    private String fromUser;
    private String fromUserRole;
    @Indexed
    private String toUser;
    private String topicTitle;
    private String message;
    private String url;
    private LocalDateTime createDate;

}
