package com.blackphoenixproductions.notificationservice.infrastructure.messagebroker;

import com.blackphoenixproductions.commons.constants.Groups;
import com.blackphoenixproductions.commons.constants.Topics;
import com.blackphoenixproductions.notificationservice.domain.INotificationService;
import com.blackphoenixproductions.notificationservice.domain.model.Post;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
public class KafkaConsumer {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);
    private final INotificationService notificationService;
    private final ObjectMapper objectMapper;


    @Autowired
    public KafkaConsumer(INotificationService notificationService, ObjectMapper objectMapper) {
        this.notificationService = notificationService;
        this.objectMapper = objectMapper;
    }


    @KafkaListener(groupId = Groups.NOTIFICATIONS_CONSUMER, topics = Topics.NOTIFICATIONS)
    public void listen(String jsonPost) {
        logger.info("Receiving message : {}", jsonPost);
        try {
            Post post = objectMapper.readValue(jsonPost, Post.class);
            notificationService.notifyTopicAuthor(post);
        } catch (JsonProcessingException ex){
            logger.error("Conversion error : ", ex);
        }
    }

}
