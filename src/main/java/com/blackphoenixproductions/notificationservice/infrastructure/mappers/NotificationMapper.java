package com.blackphoenixproductions.notificationservice.infrastructure.mappers;

import com.blackphoenixproductions.notificationservice.domain.model.Notification;
import com.blackphoenixproductions.notificationservice.infrastructure.entity.NotificationEntity;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface NotificationMapper {
    @Mapping(target = "fromUser", source = "fromUser.username")
    @Mapping(target = "toUser", source = "toUser.username")
    @Mapping(target = "fromUserRole", source = "fromUser.role")
    @Mapping(target = "topicTitle", source = "post.topic.title")
    NotificationEntity notificationToNotificationEntity(Notification notification);

    @Mapping(target = "fromUser.username", source = "fromUser")
    @Mapping(target = "toUser.username", source = "toUser")
    @Mapping(target = "fromUser.role", source = "fromUserRole")
    @Mapping(target = "post.topic.title", source = "topicTitle")
    Notification notificationEntityToNotification(NotificationEntity notificationEntity);
}
