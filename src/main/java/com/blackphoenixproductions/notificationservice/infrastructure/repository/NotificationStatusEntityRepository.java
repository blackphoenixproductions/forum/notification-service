package com.blackphoenixproductions.notificationservice.infrastructure.repository;

import com.blackphoenixproductions.notificationservice.infrastructure.entity.NotificationStatusEntity;
import org.springframework.data.repository.CrudRepository;

public interface NotificationStatusEntityRepository extends CrudRepository<NotificationStatusEntity, String> {
}
