package com.blackphoenixproductions.notificationservice.infrastructure.repository;

import com.blackphoenixproductions.notificationservice.infrastructure.entity.NotificationEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface NotificationEntityRepository extends CrudRepository<NotificationEntity, String> {
    List<NotificationEntity> findAllByToUserOrderByCreateDateDesc(String username);
}
