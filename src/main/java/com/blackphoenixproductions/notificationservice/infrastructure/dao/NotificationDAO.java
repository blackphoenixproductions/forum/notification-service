package com.blackphoenixproductions.notificationservice.infrastructure.dao;

import com.blackphoenixproductions.notificationservice.domain.INotificationDAO;
import com.blackphoenixproductions.notificationservice.domain.model.Notification;
import com.blackphoenixproductions.notificationservice.infrastructure.entity.NotificationEntity;
import com.blackphoenixproductions.notificationservice.infrastructure.entity.NotificationStatusEntity;
import com.blackphoenixproductions.notificationservice.infrastructure.mappers.NotificationMapper;
import com.blackphoenixproductions.notificationservice.infrastructure.repository.NotificationEntityRepository;
import com.blackphoenixproductions.notificationservice.infrastructure.repository.NotificationStatusEntityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class NotificationDAO implements INotificationDAO {

    private final NotificationEntityRepository notificationEntityRepository;
    private final NotificationStatusEntityRepository notificationStatusEntityRepository;
    private final NotificationMapper notificationMapper;

    @Autowired
    public NotificationDAO(NotificationEntityRepository notificationEntityRepository, NotificationStatusEntityRepository notificationStatusEntityRepository, NotificationMapper notificationMapper) {
        this.notificationEntityRepository = notificationEntityRepository;
        this.notificationStatusEntityRepository = notificationStatusEntityRepository;
        this.notificationMapper = notificationMapper;
    }

    @Transactional
    @Override
    public void saveNotification(Notification notification) {
        NotificationEntity entity = notificationMapper.notificationToNotificationEntity(notification);
        notificationEntityRepository.save(entity);
        notificationStatusEntityRepository.save(new NotificationStatusEntity(notification.getToUser().getUsername(), true));
    }

    @Transactional(readOnly = true)
    @Override
    public List<Notification> getUserNotification(String username) {
        List<NotificationEntity> userNotifications = notificationEntityRepository.findAllByToUserOrderByCreateDateDesc(username);
        List<Notification> notifications = userNotifications.stream()
                .map(notificationEntity -> notificationMapper.notificationEntityToNotification(notificationEntity))
                .collect(Collectors.toList());
        return notifications;
    }

    @Transactional(readOnly = true)
    @Override
    public Boolean getUserNotificationStatus(String username) {
        Boolean status = null;
        Optional<NotificationStatusEntity> notificationsStatus = notificationStatusEntityRepository.findById(username);
        if(notificationsStatus.isPresent()){
            status = notificationsStatus.get().getNewNotification();
        }
        return status;
    }

    @Transactional
    @Override
    public void setNotificationStatus(String username, boolean showNotificationNotice) {
        notificationStatusEntityRepository.save(new NotificationStatusEntity(username, showNotificationNotice));
    }
}
